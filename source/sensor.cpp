#include <sys/time.h>
#include <sstream>
#include "sbus/sbus.h"
#include <unistd.h>

int main(int argc, char **argv)
{

	const char *cpt_filename = "sensor.cpt"; //the schema describing the component
	scomponent *com; //the component
	sendpoint *sender; //endpoints
	snode *sn, *sn2, *parent; //for building/extracting message attributes
    

    //create the component
    //  first argument is the component name (as determined specified in the cpt file)
    //  second argument is the instance name
    if (argc!=2)
        com = new scomponent("sensor","fake_device");
    else
        com = new scomponent("sensor",argv[1]);
    
    
	//sets the logging / output levels
	scomponent::set_log_level(0, LogErrors | LogWarnings);  //| LogMessages);

	//add an endpoint
	// -- corresponds to the endpoints defined in the schema file
	// --  name, source/sink, endpoint hash (obtained by running analysecpt sensor.cpt)
	sender = com->add_endpoint("sensordata", EndpointSource, "6A0F97751791");

	//if the RDC is NOT running at the local - you need to tell SBUS where it is
    //  -- by default, it looks for :50123 on the local machine
	//com->add_rdc("192.168.1.44:50123");


    //start the component (this spawns the MW process)
	com->start(cpt_filename);

	/* send some text and a random number, every 3 seconds - over and over again...*/
	int i;
	int randval;
	srand((unsigned)time(0));

	for (i=0; i < 1000; i++)
	{

        randval = (rand() % 1000 ); 		//create random value, representing the sensor reading

		/** BUILD THE MESSAGE */
		sn2 = pack("some-sensor data msg");		//pack a string
        sn = pack(randval,"someval");       //you can specify the attribute name, optionally when packing
		parent = pack(sn2, sn, "reading");  //build the msg (corresponding to the schema) -- here packing the string and int into the "reading" structure

        //send the message
		sender->emit(parent);
		printf("New reading:\n%s\n\n", parent->toxml(1)); //NB clients needn't specify each attribute name when assigning values, SBUS handles this implicitly (hence the <-> tag for the int when printing the structure)

		delete parent;
		usleep(1500000);

	}

	com->stop();
    usleep(1500000);
	delete com;
}
