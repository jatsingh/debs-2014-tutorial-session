#include <sstream>
#include <unistd.h>
#include "sbus/sbus.h"


int main(int argc, char **argv)
{
	const char *cpt_filename = "consumer.cpt";
	scomponent *com;
	sendpoint *ep, *gotmsg;
	int fd;
	multiplex *multi;
	int n = 0;

    printf("Usage: consumer (optional <address_to_map_to>)\n");
    
    int map = (argc==2);
    
    //create the componen
    //  first argument is the component name (as determined specified in the cpt file)
    //  second argument is the instance name
	com = new scomponent("consumer", "an_instance_name");

    //the level of middleware messages to output...
	scomponent::set_log_level(0, LogErrors | LogWarnings ); // | LogMessages);  //there is also LogMessages

    //add an endpoint
	// -- corresponds to the endpoints defined in the schema file
	// --  name, source/sink, endpoint hash (obtained by running analysecpt sensor.cpt)
	ep = com->add_endpoint("sensordata", EndpointSink, "6A0F97751791"); //add the endpoint sink (see somesensor.cpp)

	//if the RDC is NOT local - you need to tell SBUS where it is
    //  -- by default, it looks for :50123 on the local machine
	//com->add_rdc("192.168.1.44:50123");

    // start the component (spawns a middleware process for managing this component)
    com->start(cpt_filename);

    //Map if an address was specified, otherwise just sit there.
    if (map){
        //map to the address (first parameter)
        if(!ep->map(argv[1], NULL)) //map to the address
            printf("Failed to map address %s\n", argv[1]);
        else
            printf("Connected to address %s\n", argv[1]);
    }

	//Multiplex helps us manage the incoming messages...
    multi = new multiplex();
	multi->add(ep);

	//consume 100 readings
	while(n<100)
	{
		//wait for an input
		fd = multi->wait(1000);
		if(fd < 0)
			continue;

        // something received
		gotmsg = com->fd_to_endpoint(fd); //work out the endpoint
		if(!strcmp(gotmsg->name, "sensordata")) //check what endpoint the msg arrived on...
		{
            //process the message
			int val;
			const char *str;

			//receive the msg
			smessage *msg;
			msg = ep->rcv();

			//extract some msg values
			str = msg->tree->extract_txt("somestring");
			val = msg->tree->extract_int("someval");
			printf("\n***\nReceived message (#%d): string=%s, val=%d\n\nXML msg:\n%s\n***\n", ++n, str, val, msg->tree->toxml(true));
			delete msg;
		}
	}
    
    printf("Recived 100 messages, stopping component\n");

	ep->unmap();
    com->stop();
    usleep(1500000);
	delete multi;
	delete com;
	return 0;
}
